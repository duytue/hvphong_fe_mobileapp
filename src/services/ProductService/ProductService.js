import axios from 'axios'
import { API_URL } from '../../constants';

class ProductService {
    getProductByProductType(productTypeId) {
        console.log("ProductService -> getProductByProductType -> productTypeId", productTypeId)
        return new Promise((resolve, reject) => {
            axios.get(`${API_URL}/get-product-by-type/${productTypeId}`)
                .then(response => resolve(response))
                .catch(error => reject(error))
        })
    };

    getProductById(productId) {
        return new Promise((resolve, reject) => {
            axios.get(`${API_URL}/product/${productId}`)
                .then(response => resolve(response))
                .catch(error => reject(error))
        })
    }
}

const productService = new ProductService();

export default productService;