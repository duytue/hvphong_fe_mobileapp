import axios from 'axios'
import { API_URL } from '../../constants';

class ProductTypeService {
    getProductType() {
        return new Promise((resolve, reject) => {
            axios.get(`${API_URL}/admin/get-product-types`)
                .then(response => resolve(response))
                .catch(error => reject(error))
        })
    }
}

const productTypeService = new ProductTypeService();

export default productTypeService;