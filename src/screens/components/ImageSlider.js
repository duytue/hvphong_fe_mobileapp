import React from "react"
import { FlatListSlider } from 'react-native-flatlist-slider';

function ImageSlider({ images }) {
    return (
        < FlatListSlider
            data={images}
            imageKey='url'
            height={240}
            timer={50000}
            onPress={() => null}
            contentContainerStyle={{ paddingHorizontal: 0 }}
            indicatorContainerStyle={{ position: 'absolute', bottom: 20 }}
            indicatorActiveColor={'#8e44ad'}
            indicatorInActiveColor={'#ffffff'}
            indicatorActiveWidth={30}
            animation
            autoscroll={false}
        />
    );
};

export default ImageSlider;
