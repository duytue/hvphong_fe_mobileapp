import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Card, ListItem } from 'react-native-material-ui';

const styles = StyleSheet.create({
    textContainer: {
        paddingHorizontal: 16,
        paddingBottom: 16
    },
    tinyLogo: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2
    },
});

function TitleText({ title }) {
    return (
        <Text style={{ fontWeight: 'bold' }}>{title}</Text>
    )
};

function CardItem({ id, imageUrl, title, price, description }) {
    return (
        <Card key={id}>
            <ListItem
                leftElement={<Image
                    style={styles.tinyLogo}
                    source={{ uri: imageUrl }}
                />}
                centerElement={{
                    primaryText: <TitleText title={title} />,
                    secondaryText: `$ ${price}`,
                }}
            />
            <View style={styles.textContainer} >
                <Text numberOfLines={4} ellipsizeMode="tail">{description}</Text>
            </View>
        </Card>

    );
}

export default CardItem;