import React, { useEffect, useState, useCallback } from 'react';
import { StyleSheet, RefreshControl, TouchableOpacity } from 'react-native';
import productService from '../../services/ProductService/ProductService';
import { ScrollView } from 'react-native-gesture-handler';
import CardItem from '../components/CardItem';
import { Searchbar } from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';
import ProductDetail from "../ProductDetail/ProductDetail"
import { getFirstImage } from '../../helpers/common';

const styles = StyleSheet.create({
    textContainer: {
        paddingHorizontal: 16,
        paddingBottom: 16,
        width: 300,
        height: 50
    },
    search: {
        marginTop: 10,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 10
    },
    container: {
        flex: 1,
    },
});

function HomeScreen({ productTypeId }) {
    const [items, setItems] = useState([]);
    const [searchQuery, setSearchQuery] = React.useState('');
    const [refreshing, setRefreshing] = React.useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [productId, setProductId] = useState(null);

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        getItems(productTypeId);
    }, []);

    const filterItems = (originalItems, searchTerm) => {
        const result = originalItems.map((item) => item.title === searchTerm)
        setItems(result);
    }

    const onChangeSearch = (query) => {
        setSearchQuery(query);
    };

    const getItems = (productTypeId) => {
        productService
            .getProductByProductType(productTypeId)
            .then((result) => {
                setItems(result.data.data);
                setRefreshing(false);
            })
            .catch((error) => console.log(error));
    };

    const hanleOpenProductDetailModel = (status, productId) => {
        setProductId(productId);
        setModalVisible(status);
    }

    useEffect(() => {
        getItems(productTypeId);
    }, [productTypeId])

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView
                style={{ flex: 1, backgroundColor: '#dce1e9' }}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }
            >
                <Searchbar
                    style={styles.search}
                    placeholder="Search"
                    onChangeText={onChangeSearch}
                    value={searchQuery}
                    onSubmitEditing={() => filterItems(items, searchQuery)}
                />
                {items && items.map((item) => (
                    <TouchableOpacity onPress={() => hanleOpenProductDetailModel(true, item.id)}>
                        <CardItem
                            id={item.id}
                            title={item.title}
                            description={item.description}
                            imageUrl={item.productImages ? getFirstImage(item.productImages[0].url) : ''}
                            price={item.price}
                            key={item.id}
                        />
                    </TouchableOpacity>
                )
                )}
                <ProductDetail modalVisible={modalVisible} setModalVisible={setModalVisible} productId={productId} />
            </ScrollView>
        </SafeAreaView>
    );
}

export default HomeScreen;