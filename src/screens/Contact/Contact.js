import React from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView, TouchableOpacity, Linking } from 'react-native';
import Communications from 'react-native-communications';

const styles = StyleSheet.create({
    root: {
        flex: 1,
        alignItems: "center",
        flexDirection: "column",
    },
    topContact: {
        borderWidth: 1,
        padding: 20,
        borderRadius: 15,
        backgroundColor: "#D8D8F6",
        marginTop: 100
    },
    titleText: {
        marginBottom: 20,
        fontSize: 22
    },
    informationText: {
        marginBottom: 15,
        fontWeight: "bold",
        fontSize: 15
    },
});

function Contact() {
    return (
        <SafeAreaView style={styles.root}>
            <View style={styles.topContact}>
                <View>
                    <Text style={styles.titleText}>Mọi chi tiết xin vui lòng liên hệ</Text>
                    <Text style={styles.informationText}>Tên: Hứa Vũ Phong</Text>
                    <TouchableOpacity
                        onPress={() => Communications.phonecall('0986842611', true)}
                    >
                        <Text style={{ ...styles.informationText, textDecorationLine: "underline", color: "blue" }}>Số điện thoại: 0986842611</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => Communications.email(['h.v.phongtv@gmail.com'], null, null, 'Tôi cần hỗ trợ', 'Hi Phong, ')}
                    >
                        <Text style={{ ...styles.informationText, textDecorationLine: "underline", color: "blue" }}>Email: h.v.phongtv@gmail.com</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => Linking.openURL('http://bit.ly/admin_tvbds')}
                    >
                        <Text style={{ ...styles.informationText, textDecorationLine: "underline", color: "blue" }}>Link up: http://bit.ly/admin_tvbds</Text>
                    </TouchableOpacity>
                </View>

            </View>
        </SafeAreaView>
    );
}

export default Contact;