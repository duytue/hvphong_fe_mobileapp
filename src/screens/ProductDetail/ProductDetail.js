import React, { useState, useEffect } from "react";
import {
    Alert,
    Modal,
    StyleSheet,
    Text,
    View,
    ScrollView
} from "react-native";
import productService from "../../services/ProductService/ProductService";
import ImageSlider from "../components/ImageSlider";
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity } from "react-native";
import { splitImageUrls } from "../../helpers/common";

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        flexDirection: "column"
    },
    modalView: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: "white",
        borderRadius: 8,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flex: 1,
        flexDirection: "column",
        maxHeight: 550
    },
    openButton: {
        padding: 12
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "left"
    },
    titleText: {
        marginBottom: 15,
        textAlign: "left",
        fontWeight: "bold"
    }
});

const ProductDetail = ({ modalVisible, setModalVisible, productId }) => {
    const [product, setProduct] = useState({});

    useEffect(() => {
        productService.getProductById(productId)
            .then(result => {
                setProduct(result.data.data)
            })
    }, [productId])

    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                {product && (
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <View style={{ flexDirection: "row" }}>
                                <View style={{ flex: 0.5, alignItems: "flex-start" }}>
                                    <Text style={styles.titleText}>Tên sản phầm: </Text>
                                    <Text style={styles.titleText}>Diện tích: </Text>
                                    <Text style={styles.titleText}>Khu vực: </Text>
                                    <Text style={styles.titleText}>Giá: </Text>
                                </View>
                                <View style={{ flex: 0.5, alignItems: "flex-start" }}>
                                    <Text style={styles.modalText}>{product.title}</Text>
                                    <Text style={styles.modalText}>{product.spread}</Text>
                                    <Text style={styles.modalText}>{product.area}</Text>
                                    <Text style={styles.modalText}>{product.price}</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: "column", alignItems: "center" }}>
                                <Text style={styles.titleText}>Chi tiết</Text>
                                <ScrollView style={{ maxHeight: 100, height: "auto" }}>
                                    <Text>{product.description}</Text>
                                </ScrollView>
                            </View>
                            <View style={{ marginTop: 25, maxHeight: 200 }}>
                                <ImageSlider images={product.productImages && splitImageUrls(product.productImages[0].url)} />
                            </View>
                        </View>
                        <TouchableOpacity
                            style={{
                                padding: 8,
                                borderColor: 'black',
                                borderRadius: 20,
                                borderWidth: 2,
                            }}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                        >
                            <Icon
                                name="close"
                                size={15}
                                color="black"
                            />
                        </TouchableOpacity>
                    </View>
                )}
            </Modal>
        </View >
    );
};

export default ProductDetail;
