exports.getFirstImage = (images) => {
    if (images) {
        const imageArray = images.split(',');
        return imageArray[0];
    }
    return null;
};

exports.splitImageUrls = (images) => {
    if (images) {
        const result = images.split(',');
        const imagesObj = result.map((item) => ({ 'url': item }))
        return imagesObj;
    }
};
