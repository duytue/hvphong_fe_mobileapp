import React, { useState, useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import HomeScreen from './src/screens/Home/HomeScreen'
import productTypeService from './src/services/ProductTypeService/ProductTypeService';
import Contact from './src/screens/Contact/Contact';

const Tab = createMaterialBottomTabNavigator();

function App() {
  const [tabs, setTabs] = useState([]);

  useEffect(() => {
    productTypeService.getProductType()
      .then((result) => {
        setTabs(result.data.data)
      })
  }, [])

  const tabColors = ["green", "blue", "red", "grey", "pink"]
  const tabIcons = ["home", "compass-outline", "city-variant-outline", "cash-multiple", "tag-text-outline", "transfer"]

  return (
    <NavigationContainer>
      {tabs.length > 0 && (
        <Tab.Navigator>
          {tabs.map((tab, index) => (
            <Tab.Screen
              key={tab.id}
              name={tab.description}
              children={() => <HomeScreen productTypeId={tab.id} />}
              options={{
                tabBarLabel: tab.description,
                tabBarIcon: ({ color }) => (
                  <MaterialCommunityIcons name={tabIcons[index]} color={color} size={26} />
                ),
                tabBarColor: tabColors[index]
              }}
            />
          ))}
          <Tab.Screen
            name="Liên hệ"
            component={Contact}
            options={{
              tabBarLabel: "Liên hệ",
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="contact-phone" color={color} size={26} />
              ),
              tabBarColor: "purple",
            }}
          />
        </Tab.Navigator>
      )}
    </NavigationContainer>
  )
};

export default App;